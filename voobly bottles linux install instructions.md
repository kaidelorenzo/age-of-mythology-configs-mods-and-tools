1. Copy the contents of both install discs for vanilla into a single directory
2. Copy the titans install disc contents into its own folder
3. Install [Bottles flatpak](https://flathub.org/apps/details/com.usebottles.bottles)
4. Download this [patched wine build](https://github.com/kaidelorenzo/aom-voobly-wine-patch/releases/download/7.6/patched-caffe-7.6-for-aom-voobly-x86_64.tar.xz)
5. Extract the build into the runners directory of the Bottles install directory (make sure the path to `wine64` looks mostly like this `com.usebottles.bottles/data/bottles/runners/patched-caffe-7.6-for-aom-voobly/bin/wine64`)
6. Launch Bottles and create a new "Gaming" bottle
7. Go to the settings and change the runner component to `patched-caffe-7.6-for-aom-voobly`
8. Install the `vcredist6` dependency
9. Run the [Voobly installer](https://www.voobly.com) in the bottle
10. Copy the contents of the first install CD to a directory that Bottles has access to 
11. Run the `setup.exe` program
12. When the installer asks to insert disc 2 remove the disc 1 files from the directory and copy the files from the second disc
13. Run the [patch](/patches/aom10to110.exe) for vanilla
14. Copy the contents of the titans install CD to a directory that Bottles has access to 
15. Run the `setup.exe` program
16. Run the [patch](/patches/aomx10to103.exe) for the titans
17. Apply any modifications or configurations to the game or Voobly and enjoy
    - Checkout my mods and use them because they are great
