Hi this is all of my Age of Mythology work

- [UI mod](/game directory structure/Age of Mythology/data)
- [Graphics fix vanilla](/game directory structure/Age of Mythology/gfxconfig)
- [Graphics fix titans](/game directory structure/Age of Mythology/gfxconfig2)
- [Larger fonts](/game directory structure/Age of Mythology/fonts)
- [Game configuration](/game directory structure/Age of Mythology/startup)
- [Reference mods](/other reference mods)
- [Official game patches](/patches)
- [Modding tools](/tools)
- [Voobly config assets](/voobly)
- [Wine patch](/aom-voobly-wine-patch.patch) to get Age of Mythology on Voobly working through wine (yes for some reason making a copy of that API fixed things)
- [Instructions](/voobly bottles linux install instructions.md) for installing Age of Mythology and Voobly in Bottles on Linux
- [Instructions](/lan over vpn on linux instructions.md) for lan over vpn on linux

### Other tools
- https://forums.ageofempires.com/t/v-0-4-resource-manager-viewing-comparing-creating-and-extracting-files-from-age-of-empires-iii-bar-archive/103349
- https://github.com/ultimate-research/xmb_lib
- https://aoe3.heavengames.com/downloads/getfile.php?id=3686&s=08c5a6f8f71011fe923c3804869cf1bd&r=https%3A//aoe3.heavengames.com/downloads/showfile.php%3Ffileid%3D3686

### Other fixes
- https://www.reddit.com/r/AgeofMythology/comments/ndukmy/og_age_of_mythology_on_windows_10_ways_to_fix/
- https://www.protondb.com/app/266840#mcfX4V4h8R

### A rust app?
Ah very astute of you. This repo is in fact a rust crate. The crate is unfinished. The end goal will be a tool that can convert Age of Mythology UI xml files to unstretch them for different aspect ratios. It's not so good right now but feel free to take a look. I may get around to making it nice at some point. However, it served its immediate purpose to me. And most people have 16:9 screens anyway.

##### Someone's installer dumps
- https://archive.org/details/aom-pc-redump
