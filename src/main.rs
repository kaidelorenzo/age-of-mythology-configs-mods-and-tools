use std::fs;
use regex::{Regex,Captures};

fn main() {
    let (offset, scalar) = (128, 0.75);
    println!("Hello, world!");
    //let path = "/var/home/kaidelorenzo/.var/app/com.usebottles.bottles/data/bottles/bottles/Age-of-Mythology/drive_c/Program Files/Microsoft Games/Age of Mythology/data/";
    let path = "";
    let data = match fs::read_to_string(format!("{}default_titans_uimain.xml", path)) {
        Ok(f) => f,
        Err(e) => {
            eprintln!(
                "failed to read from file {}",
                e
            );
            std::process::exit(1);
        }
    };
    let regex = Regex::new(r"size1024=\U{0022}(\d+) (\d+) (\d+) (\d+)\U{0022}").unwrap();
    let result = regex.replace_all(&data, |caps: &Captures| {
        format!("size1024=\"{} {} {} {}\"", scale_and_shift(&caps[1], scalar, offset, f64::floor), &caps[2], scale_and_shift(&caps[3], scalar, offset, f64::ceil), &caps[4])
    });
    // let regex = Regex::new(r"size1024=\U{0022}(\d+) (\d+)\U{0022}").unwrap();
    // let result = regex.replace_all(&result, |caps: &Captures| {
    //     format!("size1024=\"{} {}\"", scale(&caps[1], scalar), &caps[2])
    // });

    match fs::write(format!("{}auto_titans_uimain.xml", path), result.as_ref()) {
        Ok(_) => {}
        Err(e) => {
            eprintln!(
                "{} failed to write to file",
                e
            );
            std::process::exit(1);
        }
    }
}

// fn scale(string: &str, scalar: f64) -> String {
//     let num = string.parse::<f64>().unwrap();
//     let scaled = (num * scalar).round();
//     scaled.to_string()
// }

fn scale_and_shift(string: &str, scalar: f64, offset: i64, round: fn (f64) -> f64) -> String {
    let num = string.parse::<f64>().unwrap();
    let scaled = round(num * scalar) as i64;
    let shifted = scaled + offset;
    shifted.to_string()
}