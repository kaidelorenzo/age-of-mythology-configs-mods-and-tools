`default*.xml` are the original files taken from the game install. `fonts2.xml` is for vanilla Age of Mythology. `xfonts.xml` is for Age of Mythology: The Titans.

The files are modified by changing the resolution override of 1280 to be an override for 1920. 

This makes fonts in game a little bit bigger when running on a widescreen 1920x1080 display. To get some similar results for a different x dimension instead of replacing with 1920 replace with the resolution you are running the game at.

Additionally the font `Courier Bold` has an override added to set to 16 for 1920. This makes the better favor counter UI mod option work.
