`user.con` is for per user commands to run on startup. The version here simply maps `esc` to the pause function so that it works for pasuing in addition to the acutal pause key.

`user.cfg` is the per user launch configuration file. My version is annotated with what each setting does.