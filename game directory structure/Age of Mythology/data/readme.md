These files specify the main gameplay UI. The game reads `uimain.xml` no matter which version loads. That means that you need to switch between the proper version if switching between vanilla and the titans. This mod only works on 16:9 screen.

These are the changes and limitations of the mod:

1. Reverses the HUD widescreen stretch by scaling down elements. Use the rust program to scale to a custom aspect ratio.
    - the selection/queue/garrison section is not fixed. I couldn't figure out how to fix it.
2. The town bell banner is replaced with an idle scouts banner. Just look for idle villagers to find active town bells
3. There are two icons for idle transport ships and Caladria in the top right. Click them to find the idles
4. There are 3 icons for farms, herdables, and ox carts in the bottom left by the resources. Compare the number of farms to the number of villagers to figure out if there are idle farms to use.
5. There are gather counts by each reasource.
    - Favor only works for Greek. There is a hack that gets it working for other civs but you would need to use my font files and uncomment the correct tags.
6. I would love to include a way of seeing the total number of caravans/fishing ships/villagers. This would help a lot when managing economy.
7. There's also maybe a better way to get favor counting working for all civs that is less fragile. Let me know if you know of a fix
8. Lastly, it would be cool to convert other sections of the UI. If you'd be intersted I'd be happy to chat about helping out. I'm unlikely to do it on my own. 
